# translated ecommerce artikel

Artikel ecommerce yang saya translate (Private)

## 01/2017
* [Berikut adalah cara untuk memilih website builder untuk toko e-commerce Anda](https://gitlab.com/josephin-ong/translated-ecommerce-artikel/blob/master/2017/01/02-berikut-adalah-cara-untuk-memilih-website-builder-untuk-toko-e-commerce-anda.md)
* [Pengiklan cinta terprogram, tetapi apa yang tentang penerbit?](https://gitlab.com/josephin-ong/translated-ecommerce-artikel/blob/master/2017/01/pengiklan-cinta-terprogram-tetapi-apa-yang-tentang-penerbit.md')
* [10 Trik untuk ‘Call to Action’ yang Bagus](https://gitlab.com/josephin-ong/translated-ecommerce-artikel/blob/master/2017/01/10-trik-untuk-call-to-action-yang-bagus.md)
* [Tips pilih domain](https://gitlab.com/josephin-ong/translated-ecommerce-artikel/blob/master/2017/01/tips-pilih-domain.md)
* [Cek Resi dan Cek Tarif](https://gitlab.com/josephin-ong/translated-ecommerce-artikel/blob/master/2017/01/cek.md)
